package com.twuc.webApp.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, String> {
    // TODO
    //
    // 如果需要请在此添加方法。
    //
    // <--start--
    List<Product> findByProductLine(ProductLine productLine);

    List<Product> findByQuantityInStockBetween(short greater, short less);

    List<Product> findByQuantityInStockBetweenOrderByProductCode(short greater, short less);

    List<Product> findTop3ByQuantityInStockBetweenOrderByProductCode(short greater, short less);

    Page<Product> findTop3ByQuantityInStockBetweenOrderByProductCode(short greater, short less, Pageable pageable);
    // --end-->
}